# KUBECONSUL


## POSTGRES

Following https://adamtheautomator.com/postgres-to-kubernetes/#Adding_a_Helm_Chart_Repository

```
cd deploy/postgres-install
microk8s helm install postgresql-dev -f values.yaml bitnami/postgresql
```

Sicherstellen dass 

```
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=$HOME/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson
```