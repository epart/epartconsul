{{/*
Create a unique app name
*/}}
{{- define "codeconsule.name" -}}
{{- printf "%s-%s" .Chart.Name .Release.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "codeconsule.labels" -}}
{{ include "codeconsule.selectorLabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "codeconsule.selectorLabels" -}}
app: {{ include "codeconsule.name" . }}
{{- end }}
